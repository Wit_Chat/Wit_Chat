class Tree{
	public String val;
	public Tree left;
	public Tree mid;
	public Tree right;
}
//path: a string indicating the cvs file path(The 
//		first column of the file should be the title)
//The return type is the Tree Class above.
public Tree GetTree(String path){
	try {
		Tree head = new Tree();
		BufferedReader br = new BufferedReader(new FileReader(path));
		String str = br.readLine();
		int featurenum = str.split(",").length-1;
		str = br.readLine();
		while(str!=null){
			Tree treepointer = head;
			String[] arr = str.split(",");
			for(int i=0; i<featurenum; i++){
				if(arr[i].equals("-1")){
					if(treepointer.left==null){
						treepointer.left = new Tree();
					}
					treepointer = treepointer.left;
				}
				else if(arr[i].equals("0")){
					if(treepointer.mid==null){
						treepointer.mid = new Tree();
					}
					treepointer = treepointer.mid;
				}
				else{
					if(treepointer.right==null){
						treepointer.right = new Tree();
					}
					treepointer = treepointer.right;
				}
			}
			String sentence = "";
			for(int i=featurenum; i<arr.length; i++){
				sentence += arr[i];
			}
			treepointer.val = sentence;
			str = br.readLine();
		}
		return head;
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("Read file error.");
		return null;
	}
}