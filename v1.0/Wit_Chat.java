/* Project Wit_Chat
 * Author : Satyapriya Krishna , Weidong Yuan, Hugo Angulo
 * Version 0.0.0.0.1 (Most basic model)
 */


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;



public class Wit_Chat {
		
		// HardCoded Numbers for each features 
		//Horror : 0 
		//Action : 1
		//Romantic = 2
		// Actor = 3
		// Director = 4
		// Rating = 5
	
	
	
	public static String getMatch(int[] vector, TreeNode root){
		if(root.left == null && root.right == null && root.mid == null){
			return root.val;
		}
		// Condition
		
		for(int i = 0 ; i < vector.length ; i++){
			if(vector[i] == 0){
				root = root.mid;
			} else {
				root = root.right;
			}
		}
		
		return root.val;
		
		
		
	}
	
	
	
	public static int[] buildVector(HashSet<String> split , List<String> words){
		int[] res = new int[6];
		
		for(String s : words){
			if(split.contains(s) == true){
				switch(s){
				case "horror":
					res[0] = 1;
					break;
				case "action":
					res[1] = 1;
					break;
				case "romantic":
					res[2] = 1;
					break;
				case "actor":
					res[3] = 1;
					break;
				case "director":
					res[4] = 1;
					break;
				case "rating":
					res[5] = 1;
					break;
				
				}
			}
		}
		
		
		
		
		
		return res;
		
	}
	
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		String input;
		int[] vector;
		List<String> word_collection = new ArrayList<>();
		HashSet<String> words = new HashSet<>();
		////Will use Word Net  to expand it to a bigger example.
		word_collection.add("horror");
		word_collection.add("action");
		word_collection.add("romantic");
		word_collection.add("director");
		word_collection.add("rating");
		/////////////////// ********
		
	
		
		
		//Creating the tree
		TreeNode root = buildTree();
		while(true){
			words = new HashSet<>();
			System.out.println("User: ");
			input = sc.nextLine();
			String[] splits = input.split(" ");
			for(String s: splits ){
				words.add(s);
			}
			 vector = buildVector(words, word_collection );
			
			 System.out.println(getMatch(vector, root ));
			
			
		}
		
		
		
	}

}

class TreeNode {
	String val ;
	TreeNode left;
	TreeNode mid;
	TreeNode right;
}
