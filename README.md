# MSBIC_Project
Team project for Entrepreneurial Track

### The team members are as follows:-
1. Satyapriya Krishna
2. Hugo 
3. Weidong Yuan

### Team Advisor: Anatole Gershman 
Stay Tuned! We have something awesome coming soon!

### Milestone 1

1. Designed our model for the project
2. Developed the project android plateform where we will test our approaches
3. Approach 1: Use Naive Bayes to differentiate sentences